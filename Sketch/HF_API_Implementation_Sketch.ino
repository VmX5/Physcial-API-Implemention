#define PMLED 13
#define RepLED 10
#define MacroLED 2

void setup() {
  // Open the serial port to allow the vb.net program
  // to communicate with the Arduino
  Serial.begin(9600);

  // Preparing the pins...
  pinMode(PMLED, OUTPUT);
  pinMode(RepLED, OUTPUT);
  pinMode(MacroLED, OUTPUT);
}

void loop() {
  // Don't check the data if there is none...
  if (Serial.available() > 0) {
    changeLED(Serial.read() - '0');
  }
}

void changeLED(char state) {
  if (state == 0) {
    // We'll assume any blank or other command as an "off" command for all the LEDs.
    digitalWrite(PMLED, LOW);
    digitalWrite(RepLED, LOW);
    digitalWrite(MacroLED , LOW);
  } else if (state == 1) {
    // This turns the PM LED on.
    digitalWrite(PMLED, HIGH);
  } else if (state == 2) {
    // This turns the Rep LED on.
    digitalWrite(RepLED, HIGH);
  } else if (state == 3) {
    // This turns the Macro LED on.
    digitalWrite(MacroLED, HIGH);
  } else if (state == 4) {
    // This turns the PM LED off.
    digitalWrite(PMLED, LOW);
  } else if (state == 5) {
    // This turns the Rep LED off.
    digitalWrite(RepLED, LOW);
  } else if (state == 6) {
    // This turns the Macro LED off.
    digitalWrite(MacroLED, LOW);
  }
}

