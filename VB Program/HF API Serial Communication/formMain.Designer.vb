﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class formMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.NotifyIcons = New System.Windows.Forms.NotifyIcon(Me.components)
        Me.ArduinoSerialPort = New System.IO.Ports.SerialPort(Me.components)
        Me.groupSettings = New System.Windows.Forms.GroupBox()
        Me.txtThreadID = New System.Windows.Forms.TextBox()
        Me.lblThread = New System.Windows.Forms.Label()
        Me.comboPorts = New System.Windows.Forms.ComboBox()
        Me.txtAPIKey = New System.Windows.Forms.TextBox()
        Me.lblAPIKey = New System.Windows.Forms.Label()
        Me.btnSetPort = New System.Windows.Forms.Button()
        Me.lblComPort = New System.Windows.Forms.Label()
        Me.btnRefreshPorts = New System.Windows.Forms.Button()
        Me.groupLog = New System.Windows.Forms.GroupBox()
        Me.listLog = New System.Windows.Forms.ListBox()
        Me.groupTools = New System.Windows.Forms.GroupBox()
        Me.btnLED3 = New System.Windows.Forms.Button()
        Me.btnLED1 = New System.Windows.Forms.Button()
        Me.btnLED2 = New System.Windows.Forms.Button()
        Me.btnStop = New System.Windows.Forms.Button()
        Me.btnStart = New System.Windows.Forms.Button()
        Me.groupStartStop = New System.Windows.Forms.GroupBox()
        Me.timerCheck = New System.Windows.Forms.Timer(Me.components)
        Me.groupSettings.SuspendLayout()
        Me.groupLog.SuspendLayout()
        Me.groupTools.SuspendLayout()
        Me.groupStartStop.SuspendLayout()
        Me.SuspendLayout()
        '
        'NotifyIcons
        '
        Me.NotifyIcons.Text = "NotifyIcon1"
        Me.NotifyIcons.Visible = True
        '
        'ArduinoSerialPort
        '
        Me.ArduinoSerialPort.PortName = "COM4"
        '
        'groupSettings
        '
        Me.groupSettings.Controls.Add(Me.txtThreadID)
        Me.groupSettings.Controls.Add(Me.lblThread)
        Me.groupSettings.Controls.Add(Me.comboPorts)
        Me.groupSettings.Controls.Add(Me.txtAPIKey)
        Me.groupSettings.Controls.Add(Me.lblAPIKey)
        Me.groupSettings.Controls.Add(Me.btnSetPort)
        Me.groupSettings.Controls.Add(Me.lblComPort)
        Me.groupSettings.Controls.Add(Me.btnRefreshPorts)
        Me.groupSettings.Font = New System.Drawing.Font("Verdana", 8.25!)
        Me.groupSettings.Location = New System.Drawing.Point(14, 12)
        Me.groupSettings.Name = "groupSettings"
        Me.groupSettings.Size = New System.Drawing.Size(446, 92)
        Me.groupSettings.TabIndex = 1
        Me.groupSettings.TabStop = False
        Me.groupSettings.Text = "Settings"
        '
        'txtThreadID
        '
        Me.txtThreadID.Location = New System.Drawing.Point(357, 55)
        Me.txtThreadID.Name = "txtThreadID"
        Me.txtThreadID.Size = New System.Drawing.Size(68, 21)
        Me.txtThreadID.TabIndex = 10
        Me.txtThreadID.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblThread
        '
        Me.lblThread.AutoSize = True
        Me.lblThread.Location = New System.Drawing.Point(307, 59)
        Me.lblThread.Name = "lblThread"
        Me.lblThread.Size = New System.Drawing.Size(52, 13)
        Me.lblThread.TabIndex = 9
        Me.lblThread.Text = "Thread:"
        '
        'comboPorts
        '
        Me.comboPorts.Font = New System.Drawing.Font("Verdana", 8.25!)
        Me.comboPorts.FormattingEnabled = True
        Me.comboPorts.Location = New System.Drawing.Point(86, 26)
        Me.comboPorts.Name = "comboPorts"
        Me.comboPorts.Size = New System.Drawing.Size(150, 21)
        Me.comboPorts.TabIndex = 2
        '
        'txtAPIKey
        '
        Me.txtAPIKey.Font = New System.Drawing.Font("Verdana", 8.25!)
        Me.txtAPIKey.Location = New System.Drawing.Point(72, 55)
        Me.txtAPIKey.Name = "txtAPIKey"
        Me.txtAPIKey.Size = New System.Drawing.Size(229, 21)
        Me.txtAPIKey.TabIndex = 6
        Me.txtAPIKey.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblAPIKey
        '
        Me.lblAPIKey.AutoSize = True
        Me.lblAPIKey.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAPIKey.Location = New System.Drawing.Point(12, 58)
        Me.lblAPIKey.Name = "lblAPIKey"
        Me.lblAPIKey.Size = New System.Drawing.Size(58, 13)
        Me.lblAPIKey.TabIndex = 8
        Me.lblAPIKey.Text = "API Key:"
        '
        'btnSetPort
        '
        Me.btnSetPort.Font = New System.Drawing.Font("Verdana", 8.25!)
        Me.btnSetPort.Location = New System.Drawing.Point(338, 24)
        Me.btnSetPort.Name = "btnSetPort"
        Me.btnSetPort.Size = New System.Drawing.Size(87, 23)
        Me.btnSetPort.TabIndex = 4
        Me.btnSetPort.Text = "Set"
        Me.btnSetPort.UseVisualStyleBackColor = True
        '
        'lblComPort
        '
        Me.lblComPort.AutoSize = True
        Me.lblComPort.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblComPort.Location = New System.Drawing.Point(12, 29)
        Me.lblComPort.Name = "lblComPort"
        Me.lblComPort.Size = New System.Drawing.Size(66, 13)
        Me.lblComPort.TabIndex = 7
        Me.lblComPort.Text = "COM Port:"
        '
        'btnRefreshPorts
        '
        Me.btnRefreshPorts.Font = New System.Drawing.Font("Verdana", 8.25!)
        Me.btnRefreshPorts.Location = New System.Drawing.Point(244, 24)
        Me.btnRefreshPorts.Name = "btnRefreshPorts"
        Me.btnRefreshPorts.Size = New System.Drawing.Size(87, 23)
        Me.btnRefreshPorts.TabIndex = 3
        Me.btnRefreshPorts.Text = "Refresh"
        Me.btnRefreshPorts.UseVisualStyleBackColor = True
        '
        'groupLog
        '
        Me.groupLog.Controls.Add(Me.listLog)
        Me.groupLog.Font = New System.Drawing.Font("Verdana", 8.25!)
        Me.groupLog.Location = New System.Drawing.Point(14, 179)
        Me.groupLog.Name = "groupLog"
        Me.groupLog.Size = New System.Drawing.Size(446, 127)
        Me.groupLog.TabIndex = 0
        Me.groupLog.TabStop = False
        Me.groupLog.Text = "Log"
        '
        'listLog
        '
        Me.listLog.Font = New System.Drawing.Font("Verdana", 8.25!)
        Me.listLog.FormattingEnabled = True
        Me.listLog.Location = New System.Drawing.Point(12, 21)
        Me.listLog.Name = "listLog"
        Me.listLog.Size = New System.Drawing.Size(422, 95)
        Me.listLog.TabIndex = 2
        '
        'groupTools
        '
        Me.groupTools.Controls.Add(Me.btnLED3)
        Me.groupTools.Controls.Add(Me.btnLED1)
        Me.groupTools.Controls.Add(Me.btnLED2)
        Me.groupTools.Font = New System.Drawing.Font("Verdana", 8.25!)
        Me.groupTools.Location = New System.Drawing.Point(14, 110)
        Me.groupTools.Name = "groupTools"
        Me.groupTools.Size = New System.Drawing.Size(446, 63)
        Me.groupTools.TabIndex = 0
        Me.groupTools.TabStop = False
        Me.groupTools.Text = "Tools"
        '
        'btnLED3
        '
        Me.btnLED3.Location = New System.Drawing.Point(299, 24)
        Me.btnLED3.Name = "btnLED3"
        Me.btnLED3.Size = New System.Drawing.Size(110, 23)
        Me.btnLED3.TabIndex = 4
        Me.btnLED3.Text = "LED 3 Toggle"
        Me.btnLED3.UseVisualStyleBackColor = True
        '
        'btnLED1
        '
        Me.btnLED1.Location = New System.Drawing.Point(40, 24)
        Me.btnLED1.Name = "btnLED1"
        Me.btnLED1.Size = New System.Drawing.Size(110, 23)
        Me.btnLED1.TabIndex = 2
        Me.btnLED1.Text = "LED 1 Toggle"
        Me.btnLED1.UseVisualStyleBackColor = True
        '
        'btnLED2
        '
        Me.btnLED2.Location = New System.Drawing.Point(171, 24)
        Me.btnLED2.Name = "btnLED2"
        Me.btnLED2.Size = New System.Drawing.Size(110, 23)
        Me.btnLED2.TabIndex = 3
        Me.btnLED2.Text = "LED 2 Toggle"
        Me.btnLED2.UseVisualStyleBackColor = True
        '
        'btnStop
        '
        Me.btnStop.Enabled = False
        Me.btnStop.Location = New System.Drawing.Point(138, 21)
        Me.btnStop.Name = "btnStop"
        Me.btnStop.Size = New System.Drawing.Size(75, 23)
        Me.btnStop.TabIndex = 2
        Me.btnStop.Text = "Stop"
        Me.btnStop.UseVisualStyleBackColor = True
        '
        'btnStart
        '
        Me.btnStart.Location = New System.Drawing.Point(219, 21)
        Me.btnStart.Name = "btnStart"
        Me.btnStart.Size = New System.Drawing.Size(75, 23)
        Me.btnStart.TabIndex = 3
        Me.btnStart.Text = "Start"
        Me.btnStart.UseVisualStyleBackColor = True
        '
        'groupStartStop
        '
        Me.groupStartStop.Controls.Add(Me.btnStart)
        Me.groupStartStop.Controls.Add(Me.btnStop)
        Me.groupStartStop.Font = New System.Drawing.Font("Verdana", 8.25!)
        Me.groupStartStop.Location = New System.Drawing.Point(14, 312)
        Me.groupStartStop.Name = "groupStartStop"
        Me.groupStartStop.Size = New System.Drawing.Size(446, 56)
        Me.groupStartStop.TabIndex = 3
        Me.groupStartStop.TabStop = False
        '
        'timerCheck
        '
        Me.timerCheck.Interval = 2000
        '
        'formMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(475, 378)
        Me.Controls.Add(Me.groupStartStop)
        Me.Controls.Add(Me.groupLog)
        Me.Controls.Add(Me.groupTools)
        Me.Controls.Add(Me.groupSettings)
        Me.Font = New System.Drawing.Font("Verdana", 8.25!)
        Me.Name = "formMain"
        Me.Text = "HF API Serial Communication"
        Me.groupSettings.ResumeLayout(False)
        Me.groupSettings.PerformLayout()
        Me.groupLog.ResumeLayout(False)
        Me.groupTools.ResumeLayout(False)
        Me.groupStartStop.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents NotifyIcons As NotifyIcon
    Friend WithEvents ArduinoSerialPort As IO.Ports.SerialPort
    Friend WithEvents groupSettings As GroupBox
    Friend WithEvents comboPorts As ComboBox
    Friend WithEvents txtAPIKey As TextBox
    Friend WithEvents lblAPIKey As Label
    Friend WithEvents btnSetPort As Button
    Friend WithEvents lblComPort As Label
    Friend WithEvents btnRefreshPorts As Button
    Friend WithEvents groupLog As GroupBox
    Friend WithEvents listLog As ListBox
    Friend WithEvents groupTools As GroupBox
    Friend WithEvents btnLED3 As Button
    Friend WithEvents btnLED1 As Button
    Friend WithEvents btnLED2 As Button
    Friend WithEvents btnStop As Button
    Friend WithEvents btnStart As Button
    Friend WithEvents groupStartStop As GroupBox
    Friend WithEvents timerCheck As Timer
    Friend WithEvents txtThreadID As TextBox
    Friend WithEvents lblThread As Label
End Class
