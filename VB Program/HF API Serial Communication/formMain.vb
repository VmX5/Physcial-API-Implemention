﻿Imports System.Net
Imports Newtonsoft.Json.Linq

Public Class formMain

    Dim HFClient As New WebClient

    Dim led1Status As Boolean = False
    Dim led2Status As Boolean = False
    Dim led3Status As Boolean = False

    Dim threadReplies As Integer = 0

    Private Sub formMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ArduinoSerialPort.Encoding = System.Text.Encoding.Default
        btnRefreshPorts.PerformClick()
    End Sub

    Private Sub btnStart_Click(sender As Object, e As EventArgs) Handles btnStart.Click
        ArduinoSerialPort.Open()
        ArduinoSerialPort.Write(0)
        btnStart.Enabled = False
        btnStop.Enabled = True
        timerCheck.Start()
    End Sub

    Private Sub btnStop_Click(sender As Object, e As EventArgs) Handles btnStop.Click
        ArduinoSerialPort.Write(0)
        ArduinoSerialPort.Close()
        btnStart.Enabled = True
        btnStop.Enabled = False
        timerCheck.Stop()
    End Sub

    Private Sub btnLED1_Click(sender As Object, e As EventArgs) Handles btnLED1.Click
        If led1Status Then
            LED1Off()
        Else
            LED1On()
        End If
    End Sub

    Private Sub btnLED2_Click(sender As Object, e As EventArgs) Handles btnLED2.Click
        If led2Status Then
            LED2Off()
        Else
            LED2On()
        End If
    End Sub

    Private Sub btnLED3_Click(sender As Object, e As EventArgs) Handles btnLED3.Click
        If led3Status Then
            LED3Off()
        Else
            LED3On()
        End If
    End Sub

    Public Sub LED1On()
        ArduinoSerialPort.Write(1)
        led1Status = True
    End Sub

    Public Sub LED1Off()
        ArduinoSerialPort.Write(4)
        led1Status = False
    End Sub

    Public Sub LED2On()
        ArduinoSerialPort.Write(2)
        led2Status = True
    End Sub

    Public Sub LED2Off()
        ArduinoSerialPort.Write(5)
        led2Status = False
    End Sub

    Public Sub LED3On()
        ArduinoSerialPort.Write(3)
        led3Status = True
    End Sub

    Public Sub LED3Off()
        ArduinoSerialPort.Write(6)
        led3Status = False
    End Sub

    Private Sub btnRefreshPorts_Click(sender As Object, e As EventArgs) Handles btnRefreshPorts.Click
        comboPorts.Items.Clear()

        For Each sp As String In My.Computer.Ports.SerialPortNames
            comboPorts.Items.Add(sp)
        Next
    End Sub

    Private Sub btnSetPort_Click(sender As Object, e As EventArgs) Handles btnSetPort.Click
        ArduinoSerialPort.PortName = comboPorts.Text
    End Sub

    Private Sub timerCheck_Tick(sender As Object, e As EventArgs) Handles timerCheck.Tick
        LEDCommand(MakeRequest)
    End Sub

    Public Function MakeRequest() As JObject
        HFClient.Credentials = New NetworkCredential(txtAPIKey.Text, "")
        HFClient.Headers.Add("user-agent", "Mozilla/5.0 (Windows; U; Windows NT 6.1; rv:2.2) Gecko/20110201")
        Return (JObject.Parse(HFClient.DownloadString($"https://hackforums.net/api/v1/thread/{txtThreadID.Text}?include=header")))
    End Function

    Public Sub LEDCommand(input As JObject)
        Dim replies As Integer = input.SelectToken("result").SelectToken("numreplies")
        Dim unreadPMs As Integer = input.SelectToken("header").SelectToken("unreadpms")

        If (unreadPMs > 0) Then
            LED1On()
        Else
            LED1Off()
        End If

        If (threadReplies = 0) Then
            threadReplies = replies
        Else
            If (threadReplies < replies) Then
                LED2On()
            Else
                LED2Off()
            End If
            threadReplies = replies
        End If

        listLog.Items.Add($"Unread PMs: {unreadPMs}, Thread Replies: {replies}")
    End Sub

    Private Sub listLog_SelectedIndexChanged(sender As Object, e As EventArgs) Handles listLog.SelectedIndexChanged
        listLog.SelectedIndex = listLog.Items.Count - 1
    End Sub
End Class